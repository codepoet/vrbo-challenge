// To parse the JSON, add this file to your project and do:
//
//   let seatGeekEvents = try SeatGeekEvents(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.seatGeekEventsTask(with: url) { seatGeekEvents, response, error in
//     if let seatGeekEvents = seatGeekEvents {
//       ...
//     }
//   }
//   task.resume()

import Foundation

struct SeatGeekEvents: Codable {
    let meta: SeatGeekMeta
//    let inHand: SeatGeekInHand
    let events: [SeatGeekEvent]

    enum CodingKeys: String, CodingKey {
        case meta = "meta"
//        case inHand = "in_hand"
        case events = "events"
    }
}

struct SeatGeekEvent: Codable {
//    let links: [JSONAny]
//    let eventPromotion: JSONNull?
    let conditional: Bool
    let isOpen: Bool
    let id: Int
    let stats: SeatGeekEventStats
    let title: String
    let announceDate: Date
    let score: Double
    let accessMethod: SeatGeekAccessMethod?
    let announcements: SeatGeekAnnouncements
    let taxonomies: [SeatGeekTaxonomy]
    let type: String
    let status: String
    let description: String
    let datetimeLocal: Date
    let visibleUntilUTC: Date
    let timeTbd: Bool
    let dateTbd: Bool
    let performers: [SeatGeekPerformer]
    let url: String
    let createdAt: Date
    let popularity: Double
    let venue: SeatGeekVenue
    let enddatetimeUTC: Date?
    let shortTitle: String
    let datetimeUTC: Date
    let datetimeTbd: Bool
    let generalAdmission: Bool?

    enum CodingKeys: String, CodingKey {
//        case links = "links"
//        case eventPromotion = "event_promotion"
        case conditional = "conditional"
        case isOpen = "is_open"
        case id = "id"
        case stats = "stats"
        case title = "title"
        case announceDate = "announce_date"
        case score = "score"
        case accessMethod = "access_method"
        case announcements = "announcements"
        case taxonomies = "taxonomies"
        case type = "type"
        case status = "status"
        case description = "description"
        case datetimeLocal = "datetime_local"
        case visibleUntilUTC = "visible_until_utc"
        case timeTbd = "time_tbd"
        case dateTbd = "date_tbd"
        case performers = "performers"
        case url = "url"
        case createdAt = "created_at"
        case popularity = "popularity"
        case venue = "venue"
        case enddatetimeUTC = "enddatetime_utc"
        case shortTitle = "short_title"
        case datetimeUTC = "datetime_utc"
        case datetimeTbd = "datetime_tbd"
        case generalAdmission = "general_admission"
    }
}

struct SeatGeekAccessMethod: Codable {
    let employeeOnly: Bool
    let createdAt: Date
    let method: String

    enum CodingKeys: String, CodingKey {
        case employeeOnly = "employee_only"
        case createdAt = "created_at"
        case method = "method"
    }
}

struct SeatGeekAnnouncements: Codable {
    let checkoutDisclosures: SeatGeekCheckoutDisclosures?

    enum CodingKeys: String, CodingKey {
        case checkoutDisclosures = "checkout_disclosures"
    }
}

struct SeatGeekCheckoutDisclosures: Codable {
    let messages: [SeatGeekMessage]

    enum CodingKeys: String, CodingKey {
        case messages = "messages"
    }
}

struct SeatGeekMessage: Codable {
    let text: String

    enum CodingKeys: String, CodingKey {
        case text = "text"
    }
}

struct SeatGeekPerformer: Codable {
    let image: String?
    let primary: Bool?
    let colors: SeatGeekColors?
    let images: SeatGeekPerformerImages
    let hasUpcomingEvents: Bool
    let id: Int
    let stats: SeatGeekPerformerStats
    let imageLicense: String?
    let score: Double
    let location: SeatGeekLocation?
    let taxonomies: [SeatGeekTaxonomy]
    let type: String
    let numUpcomingEvents: Int
    let shortName: String
    let homeVenueID: Int?
    let slug: String
    let divisions: [SeatGeekDivision]?
    let homeTeam: Bool?
    let name: String
    let url: String
    let popularity: Int
    let imageAttribution: String?
    let awayTeam: Bool?
    let genres: [SeatGeekGenre]?

    enum CodingKeys: String, CodingKey {
        case image = "image"
        case primary = "primary"
        case colors = "colors"
        case images = "images"
        case hasUpcomingEvents = "has_upcoming_events"
        case id = "id"
        case stats = "stats"
        case imageLicense = "image_license"
        case score = "score"
        case location = "location"
        case taxonomies = "taxonomies"
        case type = "type"
        case numUpcomingEvents = "num_upcoming_events"
        case shortName = "short_name"
        case homeVenueID = "home_venue_id"
        case slug = "slug"
        case divisions = "divisions"
        case homeTeam = "home_team"
        case name = "name"
        case url = "url"
        case popularity = "popularity"
        case imageAttribution = "image_attribution"
        case awayTeam = "away_team"
        case genres = "genres"
    }
}

struct SeatGeekColors: Codable {
    let all: [String]
    let iconic: String
    let primary: [String]

    enum CodingKeys: String, CodingKey {
        case all = "all"
        case iconic = "iconic"
        case primary = "primary"
    }
}

struct SeatGeekDivision: Codable {
    let displayName: String
    let shortName: String?
    let divisionLevel: Int
    let displayType: String
    let taxonomyID: Int
    let slug: String?

    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case shortName = "short_name"
        case divisionLevel = "division_level"
        case displayType = "display_type"
        case taxonomyID = "taxonomy_id"
        case slug = "slug"
    }
}

struct SeatGeekGenre: Codable {
    let name: String
    let image: String?
    let primary: Bool
    let id: Int
    let images: SeatGeekGenreImages?
    let documentSource: SeatGeekDocumentSource
    let slug: String

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
        case primary = "primary"
        case id = "id"
        case images = "images"
        case documentSource = "document_source"
        case slug = "slug"
    }
}

struct SeatGeekDocumentSource: Codable {
    let sourceType: String
    let generationType: String

    enum CodingKeys: String, CodingKey {
        case sourceType = "source_type"
        case generationType = "generation_type"
    }
}

struct SeatGeekGenreImages: Codable {
    let the500_700: String
    let huge: String
    let fb100X72: String
    let criteo170_235: String
    let mongo: String
    let the800X320: String
    let fb600_315: String
    let ipadMiniExplore: String
    let criteo130_160: String
    let the1200X525: String
    let criteo205_100: String
    let the1200X627: String
    let squareMid: String
    let triggitFbAd: String
    let ipadEventModal: String
    let criteo400_300: String
    let the136X136: String
    let banner: String
    let block: String
    let ipadHeader: String

    enum CodingKeys: String, CodingKey {
        case the500_700 = "500_700"
        case huge = "huge"
        case fb100X72 = "fb_100x72"
        case criteo170_235 = "criteo_170_235"
        case mongo = "mongo"
        case the800X320 = "800x320"
        case fb600_315 = "fb_600_315"
        case ipadMiniExplore = "ipad_mini_explore"
        case criteo130_160 = "criteo_130_160"
        case the1200X525 = "1200x525"
        case criteo205_100 = "criteo_205_100"
        case the1200X627 = "1200x627"
        case squareMid = "square_mid"
        case triggitFbAd = "triggit_fb_ad"
        case ipadEventModal = "ipad_event_modal"
        case criteo400_300 = "criteo_400_300"
        case the136X136 = "136x136"
        case banner = "banner"
        case block = "block"
        case ipadHeader = "ipad_header"
    }
}

struct SeatGeekPerformerImages: Codable {
    let huge: String?

    enum CodingKeys: String, CodingKey {
        case huge = "huge"
    }
}

struct SeatGeekLocation: Codable {
    let lat: Double
    let lon: Double

    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lon = "lon"
    }
}

struct SeatGeekPerformerStats: Codable {
    let eventCount: Int

    enum CodingKeys: String, CodingKey {
        case eventCount = "event_count"
    }
}

struct SeatGeekTaxonomy: Codable {
    let name: String
    let parentID: Int?
    let id: Int
    let documentSource: SeatGeekDocumentSource?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case parentID = "parent_id"
        case id = "id"
        case documentSource = "document_source"
    }
}

struct SeatGeekEventStats: Codable {
    let visibleListingCount: Int?
    let dqBucketCounts: [Int]?
    let averagePrice: Double?
    let lowestPriceGoodDeals: Int?
    let medianPrice: Double?
    let listingCount: Int?
    let lowestPrice: Double?
    let highestPrice: Double?

    enum CodingKeys: String, CodingKey {
        case visibleListingCount = "visible_listing_count"
        case dqBucketCounts = "dq_bucket_counts"
        case averagePrice = "average_price"
        case lowestPriceGoodDeals = "lowest_price_good_deals"
        case medianPrice = "median_price"
        case listingCount = "listing_count"
        case lowestPrice = "lowest_price"
        case highestPrice = "highest_price"
    }
}

struct SeatGeekVenue: Codable {
//    let links: [JSONAny]
    let metroCode: Int
    let postalCode: String
    let timezone: String
    let hasUpcomingEvents: Bool
    let id: Int
    let city: String
    let extendedAddress: String
    let displayLocation: String
    let state: String?
    let score: Double
    let location: SeatGeekLocation
    let accessMethod: SeatGeekAccessMethod?
    let numUpcomingEvents: Int
    let address: String
    let capacity: Int
    let slug: String
    let name: String
    let url: String
    let country: String
    let popularity: Int
    let nameV2: String

    enum CodingKeys: String, CodingKey {
//        case links = "links"
        case metroCode = "metro_code"
        case postalCode = "postal_code"
        case timezone = "timezone"
        case hasUpcomingEvents = "has_upcoming_events"
        case id = "id"
        case city = "city"
        case extendedAddress = "extended_address"
        case displayLocation = "display_location"
        case state = "state"
        case score = "score"
        case location = "location"
        case accessMethod = "access_method"
        case numUpcomingEvents = "num_upcoming_events"
        case address = "address"
        case capacity = "capacity"
        case slug = "slug"
        case name = "name"
        case url = "url"
        case country = "country"
        case popularity = "popularity"
        case nameV2 = "name_v2"
    }
}

//struct SeatGeekInHand: Codable {
//}

struct SeatGeekMeta: Codable {
//    let geolocation: JSONNull?
    let perPage: Int
    let total: Int
    let took: Int
    let page: Int

    enum CodingKeys: String, CodingKey {
//        case geolocation = "geolocation"
        case perPage = "per_page"
        case total = "total"
        case took = "took"
        case page = "page"
    }
}

// MARK: Convenience initializers and mutators

extension SeatGeekEvents {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekEvents.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        meta: SeatGeekMeta? = nil,
//        inHand: SeatGeekInHand? = nil,
        events: [SeatGeekEvent]? = nil
    ) -> SeatGeekEvents {
        return SeatGeekEvents(
            meta: meta ?? self.meta,
//            inHand: inHand ?? self.inHand,
            events: events ?? self.events
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekEvent {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekEvent.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
//        links: [JSONAny]? = nil,
//        eventPromotion: JSONNull?? = nil,
        conditional: Bool? = nil,
        isOpen: Bool? = nil,
        id: Int? = nil,
        stats: SeatGeekEventStats? = nil,
        title: String? = nil,
        announceDate: Date? = nil,
        score: Double? = nil,
        accessMethod: SeatGeekAccessMethod?? = nil,
        announcements: SeatGeekAnnouncements? = nil,
        taxonomies: [SeatGeekTaxonomy]? = nil,
        type: String? = nil,
        status: String? = nil,
        description: String? = nil,
        datetimeLocal: Date? = nil,
        visibleUntilUTC: Date? = nil,
        timeTbd: Bool? = nil,
        dateTbd: Bool? = nil,
        performers: [SeatGeekPerformer]? = nil,
        url: String? = nil,
        createdAt: Date? = nil,
        popularity: Double? = nil,
        venue: SeatGeekVenue? = nil,
        enddatetimeUTC: Date? = nil,
        shortTitle: String? = nil,
        datetimeUTC: Date? = nil,
        datetimeTbd: Bool? = nil,
        generalAdmission: Bool?? = nil
    ) -> SeatGeekEvent {
        return SeatGeekEvent(
//            links: links ?? self.links,
//            eventPromotion: eventPromotion ?? self.eventPromotion,
            conditional: conditional ?? self.conditional,
            isOpen: isOpen ?? self.isOpen,
            id: id ?? self.id,
            stats: stats ?? self.stats,
            title: title ?? self.title,
            announceDate: announceDate ?? self.announceDate,
            score: score ?? self.score,
            accessMethod: accessMethod ?? self.accessMethod,
            announcements: announcements ?? self.announcements,
            taxonomies: taxonomies ?? self.taxonomies,
            type: type ?? self.type,
            status: status ?? self.status,
            description: description ?? self.description,
            datetimeLocal: datetimeLocal ?? self.datetimeLocal,
            visibleUntilUTC: visibleUntilUTC ?? self.visibleUntilUTC,
            timeTbd: timeTbd ?? self.timeTbd,
            dateTbd: dateTbd ?? self.dateTbd,
            performers: performers ?? self.performers,
            url: url ?? self.url,
            createdAt: createdAt ?? self.createdAt,
            popularity: popularity ?? self.popularity,
            venue: venue ?? self.venue,
            enddatetimeUTC: enddatetimeUTC ?? self.enddatetimeUTC,
            shortTitle: shortTitle ?? self.shortTitle,
            datetimeUTC: datetimeUTC ?? self.datetimeUTC,
            datetimeTbd: datetimeTbd ?? self.datetimeTbd,
            generalAdmission: generalAdmission ?? self.generalAdmission
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekAccessMethod {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekAccessMethod.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        employeeOnly: Bool? = nil,
        createdAt: Date? = nil,
        method: String? = nil
    ) -> SeatGeekAccessMethod {
        return SeatGeekAccessMethod(
            employeeOnly: employeeOnly ?? self.employeeOnly,
            createdAt: createdAt ?? self.createdAt,
            method: method ?? self.method
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekAnnouncements {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekAnnouncements.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        checkoutDisclosures: SeatGeekCheckoutDisclosures?? = nil
    ) -> SeatGeekAnnouncements {
        return SeatGeekAnnouncements(
            checkoutDisclosures: checkoutDisclosures ?? self.checkoutDisclosures
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekCheckoutDisclosures {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekCheckoutDisclosures.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        messages: [SeatGeekMessage]? = nil
    ) -> SeatGeekCheckoutDisclosures {
        return SeatGeekCheckoutDisclosures(
            messages: messages ?? self.messages
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekMessage {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekMessage.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        text: String? = nil
    ) -> SeatGeekMessage {
        return SeatGeekMessage(
            text: text ?? self.text
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekPerformer {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekPerformer.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        image: String?? = nil,
        primary: Bool?? = nil,
        colors: SeatGeekColors?? = nil,
        images: SeatGeekPerformerImages? = nil,
        hasUpcomingEvents: Bool? = nil,
        id: Int? = nil,
        stats: SeatGeekPerformerStats? = nil,
        imageLicense: String?? = nil,
        score: Double? = nil,
        location: SeatGeekLocation?? = nil,
        taxonomies: [SeatGeekTaxonomy]? = nil,
        type: String? = nil,
        numUpcomingEvents: Int? = nil,
        shortName: String? = nil,
        homeVenueID: Int?? = nil,
        slug: String? = nil,
        divisions: [SeatGeekDivision]?? = nil,
        homeTeam: Bool?? = nil,
        name: String? = nil,
        url: String? = nil,
        popularity: Int? = nil,
        imageAttribution: String?? = nil,
        awayTeam: Bool?? = nil,
        genres: [SeatGeekGenre]?? = nil
    ) -> SeatGeekPerformer {
        return SeatGeekPerformer(
            image: image ?? self.image,
            primary: primary ?? self.primary,
            colors: colors ?? self.colors,
            images: images ?? self.images,
            hasUpcomingEvents: hasUpcomingEvents ?? self.hasUpcomingEvents,
            id: id ?? self.id,
            stats: stats ?? self.stats,
            imageLicense: imageLicense ?? self.imageLicense,
            score: score ?? self.score,
            location: location ?? self.location,
            taxonomies: taxonomies ?? self.taxonomies,
            type: type ?? self.type,
            numUpcomingEvents: numUpcomingEvents ?? self.numUpcomingEvents,
            shortName: shortName ?? self.shortName,
            homeVenueID: homeVenueID ?? self.homeVenueID,
            slug: slug ?? self.slug,
            divisions: divisions ?? self.divisions,
            homeTeam: homeTeam ?? self.homeTeam,
            name: name ?? self.name,
            url: url ?? self.url,
            popularity: popularity ?? self.popularity,
            imageAttribution: imageAttribution ?? self.imageAttribution,
            awayTeam: awayTeam ?? self.awayTeam,
            genres: genres ?? self.genres
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekColors {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekColors.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        all: [String]? = nil,
        iconic: String? = nil,
        primary: [String]? = nil
    ) -> SeatGeekColors {
        return SeatGeekColors(
            all: all ?? self.all,
            iconic: iconic ?? self.iconic,
            primary: primary ?? self.primary
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekDivision {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekDivision.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        displayName: String? = nil,
        shortName: String?? = nil,
        divisionLevel: Int? = nil,
        displayType: String? = nil,
        taxonomyID: Int? = nil,
        slug: String?? = nil
    ) -> SeatGeekDivision {
        return SeatGeekDivision(
            displayName: displayName ?? self.displayName,
            shortName: shortName ?? self.shortName,
            divisionLevel: divisionLevel ?? self.divisionLevel,
            displayType: displayType ?? self.displayType,
            taxonomyID: taxonomyID ?? self.taxonomyID,
            slug: slug ?? self.slug
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekGenre {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekGenre.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        name: String? = nil,
        image: String?? = nil,
        primary: Bool? = nil,
        id: Int? = nil,
        images: SeatGeekGenreImages?? = nil,
        documentSource: SeatGeekDocumentSource? = nil,
        slug: String? = nil
    ) -> SeatGeekGenre {
        return SeatGeekGenre(
            name: name ?? self.name,
            image: image ?? self.image,
            primary: primary ?? self.primary,
            id: id ?? self.id,
            images: images ?? self.images,
            documentSource: documentSource ?? self.documentSource,
            slug: slug ?? self.slug
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekDocumentSource {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekDocumentSource.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        sourceType: String? = nil,
        generationType: String? = nil
    ) -> SeatGeekDocumentSource {
        return SeatGeekDocumentSource(
            sourceType: sourceType ?? self.sourceType,
            generationType: generationType ?? self.generationType
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekGenreImages {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekGenreImages.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        the500_700: String? = nil,
        huge: String? = nil,
        fb100X72: String? = nil,
        criteo170_235: String? = nil,
        mongo: String? = nil,
        the800X320: String? = nil,
        fb600_315: String? = nil,
        ipadMiniExplore: String? = nil,
        criteo130_160: String? = nil,
        the1200X525: String? = nil,
        criteo205_100: String? = nil,
        the1200X627: String? = nil,
        squareMid: String? = nil,
        triggitFbAd: String? = nil,
        ipadEventModal: String? = nil,
        criteo400_300: String? = nil,
        the136X136: String? = nil,
        banner: String? = nil,
        block: String? = nil,
        ipadHeader: String? = nil
    ) -> SeatGeekGenreImages {
        return SeatGeekGenreImages(
            the500_700: the500_700 ?? self.the500_700,
            huge: huge ?? self.huge,
            fb100X72: fb100X72 ?? self.fb100X72,
            criteo170_235: criteo170_235 ?? self.criteo170_235,
            mongo: mongo ?? self.mongo,
            the800X320: the800X320 ?? self.the800X320,
            fb600_315: fb600_315 ?? self.fb600_315,
            ipadMiniExplore: ipadMiniExplore ?? self.ipadMiniExplore,
            criteo130_160: criteo130_160 ?? self.criteo130_160,
            the1200X525: the1200X525 ?? self.the1200X525,
            criteo205_100: criteo205_100 ?? self.criteo205_100,
            the1200X627: the1200X627 ?? self.the1200X627,
            squareMid: squareMid ?? self.squareMid,
            triggitFbAd: triggitFbAd ?? self.triggitFbAd,
            ipadEventModal: ipadEventModal ?? self.ipadEventModal,
            criteo400_300: criteo400_300 ?? self.criteo400_300,
            the136X136: the136X136 ?? self.the136X136,
            banner: banner ?? self.banner,
            block: block ?? self.block,
            ipadHeader: ipadHeader ?? self.ipadHeader
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekPerformerImages {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekPerformerImages.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        huge: String?? = nil
    ) -> SeatGeekPerformerImages {
        return SeatGeekPerformerImages(
            huge: huge ?? self.huge
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekLocation {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekLocation.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        lat: Double? = nil,
        lon: Double? = nil
    ) -> SeatGeekLocation {
        return SeatGeekLocation(
            lat: lat ?? self.lat,
            lon: lon ?? self.lon
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekPerformerStats {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekPerformerStats.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        eventCount: Int? = nil
    ) -> SeatGeekPerformerStats {
        return SeatGeekPerformerStats(
            eventCount: eventCount ?? self.eventCount
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekTaxonomy {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekTaxonomy.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        name: String? = nil,
        parentID: Int?? = nil,
        id: Int? = nil,
        documentSource: SeatGeekDocumentSource?? = nil
    ) -> SeatGeekTaxonomy {
        return SeatGeekTaxonomy(
            name: name ?? self.name,
            parentID: parentID ?? self.parentID,
            id: id ?? self.id,
            documentSource: documentSource ?? self.documentSource
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekEventStats {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekEventStats.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        visibleListingCount: Int?? = nil,
        dqBucketCounts: [Int]?? = nil,
        averagePrice: Double?? = nil,
        lowestPriceGoodDeals: Int?? = nil,
        medianPrice: Double?? = nil,
        listingCount: Int?? = nil,
        lowestPrice: Double?? = nil,
        highestPrice: Double?? = nil
    ) -> SeatGeekEventStats {
        return SeatGeekEventStats(
            visibleListingCount: visibleListingCount ?? self.visibleListingCount,
            dqBucketCounts: dqBucketCounts ?? self.dqBucketCounts,
            averagePrice: averagePrice ?? self.averagePrice,
            lowestPriceGoodDeals: lowestPriceGoodDeals ?? self.lowestPriceGoodDeals,
            medianPrice: medianPrice ?? self.medianPrice,
            listingCount: listingCount ?? self.listingCount,
            lowestPrice: lowestPrice ?? self.lowestPrice,
            highestPrice: highestPrice ?? self.highestPrice
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension SeatGeekVenue {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekVenue.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
//        links: [JSONAny]? = nil,
        metroCode: Int? = nil,
        postalCode: String? = nil,
        timezone: String? = nil,
        hasUpcomingEvents: Bool? = nil,
        id: Int? = nil,
        city: String? = nil,
        extendedAddress: String? = nil,
        displayLocation: String? = nil,
        state: String?? = nil,
        score: Double? = nil,
        location: SeatGeekLocation? = nil,
        accessMethod: SeatGeekAccessMethod?? = nil,
        numUpcomingEvents: Int? = nil,
        address: String? = nil,
        capacity: Int? = nil,
        slug: String? = nil,
        name: String? = nil,
        url: String? = nil,
        country: String? = nil,
        popularity: Int? = nil,
        nameV2: String? = nil
    ) -> SeatGeekVenue {
        return SeatGeekVenue(
//            links: links ?? self.links,
            metroCode: metroCode ?? self.metroCode,
            postalCode: postalCode ?? self.postalCode,
            timezone: timezone ?? self.timezone,
            hasUpcomingEvents: hasUpcomingEvents ?? self.hasUpcomingEvents,
            id: id ?? self.id,
            city: city ?? self.city,
            extendedAddress: extendedAddress ?? self.extendedAddress,
            displayLocation: displayLocation ?? self.displayLocation,
            state: state ?? self.state,
            score: score ?? self.score,
            location: location ?? self.location,
            accessMethod: accessMethod ?? self.accessMethod,
            numUpcomingEvents: numUpcomingEvents ?? self.numUpcomingEvents,
            address: address ?? self.address,
            capacity: capacity ?? self.capacity,
            slug: slug ?? self.slug,
            name: name ?? self.name,
            url: url ?? self.url,
            country: country ?? self.country,
            popularity: popularity ?? self.popularity,
            nameV2: nameV2 ?? self.nameV2
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//extension SeatGeekInHand {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(SeatGeekInHand.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//    ) -> SeatGeekInHand {
//        return SeatGeekInHand(
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}

extension SeatGeekMeta {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SeatGeekMeta.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
//        geolocation: JSONNull?? = nil,
        perPage: Int? = nil,
        total: Int? = nil,
        took: Int? = nil,
        page: Int? = nil
    ) -> SeatGeekMeta {
        return SeatGeekMeta(
//            geolocation: geolocation ?? self.geolocation,
            perPage: perPage ?? self.perPage,
            total: total ?? self.total,
            took: took ?? self.took,
            page: page ?? self.page
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: Encode/decode helpers

//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

//class JSONAny: Codable {
//    let value: Any
//
//    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
//        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
//        return DecodingError.typeMismatch(JSONAny.self, context)
//    }
//
//    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
//        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
//        return EncodingError.invalidValue(value, context)
//    }
//
//    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if container.decodeNil() {
//            return JSONNull()
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if let value = try? container.decodeNil() {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer() {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
//        if let value = try? container.decode(Bool.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Double.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(String.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decodeNil(forKey: key) {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
//        var arr: [Any] = []
//        while !container.isAtEnd {
//            let value = try decode(from: &container)
//            arr.append(value)
//        }
//        return arr
//    }
//
//    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
//        var dict = [String: Any]()
//        for key in container.allKeys {
//            let value = try decode(from: &container, forKey: key)
//            dict[key.stringValue] = value
//        }
//        return dict
//    }
//
//    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
//        for value in array {
//            if let value = value as? Bool {
//                try container.encode(value)
//            } else if let value = value as? Int64 {
//                try container.encode(value)
//            } else if let value = value as? Double {
//                try container.encode(value)
//            } else if let value = value as? String {
//                try container.encode(value)
//            } else if value is JSONNull {
//                try container.encodeNil()
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer()
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
//        for (key, value) in dictionary {
//            let key = JSONCodingKey(stringValue: key)!
//            if let value = value as? Bool {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Int64 {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Double {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? String {
//                try container.encode(value, forKey: key)
//            } else if value is JSONNull {
//                try container.encodeNil(forKey: key)
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer(forKey: key)
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
//        if let value = value as? Bool {
//            try container.encode(value)
//        } else if let value = value as? Int64 {
//            try container.encode(value)
//        } else if let value = value as? Double {
//            try container.encode(value)
//        } else if let value = value as? String {
//            try container.encode(value)
//        } else if value is JSONNull {
//            try container.encodeNil()
//        } else {
//            throw encodingError(forValue: value, codingPath: container.codingPath)
//        }
//    }
//
//    public required init(from decoder: Decoder) throws {
//        if var arrayContainer = try? decoder.unkeyedContainer() {
//            self.value = try JSONAny.decodeArray(from: &arrayContainer)
//        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
//            self.value = try JSONAny.decodeDictionary(from: &container)
//        } else {
//            let container = try decoder.singleValueContainer()
//            self.value = try JSONAny.decode(from: container)
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        if let arr = self.value as? [Any] {
//            var container = encoder.unkeyedContainer()
//            try JSONAny.encode(to: &container, array: arr)
//        } else if let dict = self.value as? [String: Any] {
//            var container = encoder.container(keyedBy: JSONCodingKey.self)
//            try JSONAny.encode(to: &container, dictionary: dict)
//        } else {
//            var container = encoder.singleValueContainer()
//            try JSONAny.encode(to: &container, value: self.value)
//        }
//    }
//}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .custom(betterDateDecoder)
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
			do {
				let object = try newJSONDecoder().decode(T.self, from: data)
				completionHandler(object, response, nil)
			} catch {
				completionHandler(nil, response, error)
			}
        }
    }

    func seatGeekEventsTask(with url: URL, completionHandler: @escaping (SeatGeekEvents?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
