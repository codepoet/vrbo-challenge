//
//  SeatGeekAPI.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import Foundation

let apiClientID = "MTY2NzE0ODJ8MTU1ODEwMTUzNy4wNQ"
let apiSecret = "13d1b9b0d13c707c2af0b1bd00bf35520b05e52cb57b47a8258c6492fb87ec0d"

enum SeatGeekError: Error {
	case url(String)
	case network(Error)
}


/// All supported Seat Geek API calls are within.
struct SeatGeek {
	/// Executes a search query and returns up to 50 matching SeatGeekEvents.
	///
	/// - Parameters:
	///   - query: The query to send to Seat Geek, unmodified.
	///   - completion: Your handler for the results.  Be sure to check the error return.
	static func search(_ query: String, completion: @escaping (SeatGeekEvents?, Error?) -> Void) {
		var components = URLComponents(string: "https://api.seatgeek.com/2/events")!
		components.queryItems = [
			URLQueryItem(name: "client_id", value: apiClientID),
			URLQueryItem(name: "q", value: query),
			URLQueryItem(name: "per_page", value: "50")
		]
		
		guard let url = components.url else {
			completion(nil, SeatGeekError.url("Error creating URL from query: \(query)"))
			return
		}
		
		let task = URLSession.shared.seatGeekEventsTask(with: url) { (events, response, error) in
			if let error = error {
				completion(nil, SeatGeekError.network(error))
				return
			}
			completion(events, nil)
		}
		task.resume()
	}
}
