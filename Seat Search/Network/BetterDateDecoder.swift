//
//  BetterDateDecoder.swift
//  CommonUtilities
//
//  Created by Adam Knight on 8/12/18.
//  Copyright © 2018 Adam Knight. All rights reserved.
//

import Foundation

enum DateError: String, Error {
	case invalidDate
}

/// The stock Codable ISO date code covers exactly one type of ISO date that no one uses in practice.  This supports
/// that one and most of the other ISO8601 dates found in the wild.
///
/// Add to JSONDecoder as a custom date strategy to use.
///
/// - Parameter decoder: A JSONDecoder instance.
/// - Returns: A Date in the UTC timezone.
/// - Throws: DateError.invalidDate.  If I have to explain when, then I didn't name it correctly.
func betterDateDecoder(decoder: Decoder) throws -> Date {
	let formatter = DateFormatter()
	formatter.calendar = Calendar(identifier: .iso8601)
	formatter.locale = Locale(identifier: "en_US_POSIX")
	formatter.timeZone = TimeZone(secondsFromGMT: 0)
	
	let container = try decoder.singleValueContainer()
	let dateStr = try container.decode(String.self)
	
	let formats = [
		"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
		"yyyy-MM-dd'T'HH:mm:ssZ",
		"yyyy-MM-dd'T'HH:mm:ss",
		"yyyy-MM-dd HH:mm:ss ZZZ",
		"yyyy-MM-dd HH:mm:ss",
		"yyyy-MM-dd"
	]
	
	for format in formats {
		formatter.dateFormat = format
		if let date = formatter.date(from: dateStr) {
			return date
		}
	}
	
	debugPrint("Failed to parse date: \(dateStr)")
	throw DateError.invalidDate
}
