//
//  VenueLocation.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import MapKit

/// A utility class for showing a pin for an Event in MapKit.
class VenueLocation: NSObject, MKAnnotation {
	/// The point representing the venue.
	var coordinate: CLLocationCoordinate2D
	
	/// A region that is centered on the venue and will show a reasonable space around it.
	var region: MKCoordinateRegion
	
	init(event: Event) {
		let coords = event.venueCoords
		coordinate = CLLocationCoordinate2D(latitude: coords.lat, longitude: coords.lon)
		region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
	}
}

