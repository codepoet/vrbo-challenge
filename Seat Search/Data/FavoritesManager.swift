//
//  FavoritesManager.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import Foundation

/// Use this to fat-finger-proof your UserDefaults keys.
enum UserDefaultsKeys: String {
	/// Stores an encoded array of Event objects the user has marked as favorites.
	case favorites
}

/// Provides an inferface to manipulate saved/favorite Events.
struct FavoritesManager {
	static var defaults = UserDefaults.standard
	
	/// List all of this user's favorite events.
	///
	/// Note that they are simply restored and not fetched from the server
	/// so there is a chance that one or more could be out of date.
	///
	/// - Returns: An array of favorite events, or an empty array if none were found.
	static func allFavorites() -> [Event] {
		let decoder = JSONDecoder()
		
		guard let data = defaults.data(forKey: UserDefaultsKeys.favorites.rawValue) else { return [] }
		
		do {
			let favorites: [Event] = try decoder.decode([Event].self, from: data)
			return favorites
		} catch {
			debugPrint(error)
			return []
		}
	}
	
	
	/// Checks the list of favorite events for the given `Event`.
	///
	/// - Parameter event: The `Event` to test.
	/// - Returns: `true` if the Event was found; otherwise `false`.
	static func isFavorite(event: Event) -> Bool {
		let favs = allFavorites()
		return favs.contains(where: { $0.eventID == event.eventID })
	}
	
	
	/// Adds an event as a favorite.
	///
	/// - Parameter event: The event to mark as a favorite.
	/// - Returns: Returns true if the event was a new favorite event and successfully saved.
	@discardableResult
	static func addFavorite(event: Event) -> Bool {
		var favs = allFavorites()
		guard !favs.contains(where: { $0.eventID == event.eventID }) else { return false }
		favs.append(event)
		
		do {
			let encoder = JSONEncoder()
			let data = try encoder.encode(favs)
			defaults.set(data, forKey: UserDefaultsKeys.favorites.rawValue)
			return true
		} catch {
			debugPrint(error)
			return false
		}
	}

	
	/// Removes an event from the user's favorites.
	///
	/// - Parameter event: The event to remove from the user's favorites.
	/// - Returns: Returns true if the event was found and removal was successful.
	@discardableResult
	static func removeFavorite(event: Event) -> Bool {
		var favs = allFavorites()
		guard let idx = favs.firstIndex(where: { $0.eventID == event.eventID }) else { return false }
		favs.remove(at: idx)
		
		do {
			let encoder = JSONEncoder()
			let data = try encoder.encode(favs)
			defaults.set(data, forKey: UserDefaultsKeys.favorites.rawValue)
			return true
		} catch {
			debugPrint(error)
			return false
		}
	}
}
