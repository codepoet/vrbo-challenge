//
//  Event.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit
import CoreLocation

fileprivate var dateFormatter: DateFormatter!
fileprivate var imageCache = NSCache<NSString,UIImage>()

enum EventError: Error {
	case invalidURL(String)
	case noData(String)
	case invalidImage(String)
	case network(Error)
}

/// The internal view model for a given SeatGeekEvent object.
class Event: Codable {
	let eventID: Int
	let title: String
	let location: String
	let venue: String
	let venueCoords: SeatGeekLocation
	let date: Date
	let imageURL: String?

	var dateString: String
	
	required init(eventID: Int, title: String, location: String, venue: String, venueCoords: SeatGeekLocation, date: Date, imageURL: String?) {
		
		if dateFormatter == nil {
			dateFormatter = DateFormatter()
			dateFormatter.doesRelativeDateFormatting = true
			dateFormatter.dateStyle = .full
			dateFormatter.timeStyle = .short
		}
		
		self.eventID = eventID
		self.title = title
		self.location = location
		self.venue = venue
		self.venueCoords = venueCoords
		self.date = date
		self.imageURL = imageURL
		
		self.dateString = dateFormatter.string(from: self.date)
	}
	
	convenience init(event: SeatGeekEvent) {
		self.init(
			eventID: event.id,
			title: event.title,
			location: event.venue.displayLocation,
			venue: event.venue.name,
			venueCoords: event.venue.location,
			date: event.datetimeUTC,
			imageURL: event.performers.first?.image
		)
	}
	
	/// A caching downloader for venue images.
	///
	/// The first time a URL is requested, it will be downloaded.  After that, the image is stored in a cache and
	/// is keyed by the URL.  As long as the cache is not purged due to memory pressure, the application will
	/// retain the image for the duration of its runtime to speed up things like table view scrolling.
	///
	/// - Parameter completion: A handler to run when the image is returned.  Note that it could return quickly
	/// from cache or after a delay if the network is involved.  Either way, the completion block is guaranteed
	/// to be run on the main queue so UI work is safe.
	func fetchImage(completion: @escaping (UIImage?, Error?) -> Void) {
		
		guard let imageURL = self.imageURL else {
			DispatchQueue.main.async { completion(nil, EventError.invalidURL("(nil)")) }
			return
		}
		
		if let image = imageCache.object(forKey: imageURL as NSString) {
			debugPrint("Returning cached image for \(imageURL)")
			DispatchQueue.main.async { completion(image, nil) }
			return
		}
		
		guard let url = URL(string: imageURL) else {
			DispatchQueue.main.async { completion(nil, EventError.invalidURL(imageURL)) }
			return
		}
		
		URLSession.shared.dataTask(with: url) { (data, response, error) in
			if let error = error {
				DispatchQueue.main.async { completion(nil, EventError.network(error)) }
				return
			}
			
			guard let data = data else {
				DispatchQueue.main.async { completion(nil, EventError.noData(imageURL)) }
				return
			}
			
			guard let image = UIImage(data: data) else {
				DispatchQueue.main.async { completion(nil, EventError.invalidImage(imageURL)) }
				return
			}
			
			debugPrint("Fetched new image from \(imageURL)")
			imageCache.setObject(image, forKey: imageURL as NSString, cost: data.count)
			DispatchQueue.main.async { completion(image, nil) }
		}.resume() // <-- Don't forget that part.  A person could waste a whole 10m wondering what was wrong otherwise.
		// Hypothetically.
	}
}
