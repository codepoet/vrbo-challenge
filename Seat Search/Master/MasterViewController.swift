//
//  MasterViewController.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
	
	var detailViewController: DetailViewController? = nil
	var objects = [Event]()
	
	private var searchController: UISearchController!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Search Controller
		searchController = UISearchController(searchResultsController: nil)
		searchController.searchResultsUpdater = self
		searchController.dimsBackgroundDuringPresentation = false // The default is true.
		navigationItem.searchController = searchController
		definesPresentationContext = true

		// Search Bar
		let searchBar = searchController.searchBar
		searchBar.delegate = self // Monitor when the search button is tapped.
		searchBar.autocapitalizationType = .none
		searchBar.barStyle = .black
		searchBar.searchBarStyle = .minimal
		searchBar.barTintColor = .white
		searchBar.tintColor = .white
		
		// Detail View
		if let split = splitViewController {
			let controllers = split.viewControllers
			detailViewController =
				(controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
		
		// Table View
		tableView.rowHeight = UITableView.automaticDimension
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		// Drop right into the search field on launch
		searchController.searchBar.becomeFirstResponder()
		super.viewDidAppear(animated)
	}

	// MARK: - Search
	
	// If a new character led to an API call for each of some Large N number of devices you'd have brownouts in
	// datacenters.  A small delay to coalesce the input somewhat before firing off the search saves everyone
	// a bit of pain.
	var searchTimer: Timer? = nil
	
	func search(query: String) {
		// Clear out an outstanding timer event
		if let timer = searchTimer {
			timer.invalidate()
			searchTimer = nil
		}
		
		// Defer the execution of this a little so we don't hammer the API as the user types.
		searchTimer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (timer) in
			if query.count == 0 {
				// If the search was empty, load all of the user's favorites.
				self.objects = FavoritesManager.allFavorites()
				DispatchQueue.main.async { self.tableView.reloadData() }
				return
			}
			
			SeatGeek.search(query) { (events, error) in
				if let error = error {
					debugPrint("Search error: \(error)")  // FIXME: Display something to the user
					return
				}
				
				guard let events = events else {
					debugPrint("No events returned")
					return
				}
				
				DispatchQueue.main.async {
					// Grab the results and pull out the favorites so they can be put at the top.
					// This is but one way.  Another would be an in-place iterate and mutate (risky).
					// Yet another would be to iterate over one array and then build two arrays as you go.
					// As the dataset is ~50, over-thinking this will waste more time than all the phones
					// ever would.
					let allEvents = events.events.map { Event(event: $0) }
					let favoriteEvents = allEvents.filter { FavoritesManager.isFavorite(event: $0) }
					let remainder = allEvents.filter { (event) in !favoriteEvents.contains { $0.eventID == event.eventID } }
					
					// The assignment of events must happen very close to the reload to ensure the new section/row
					// counts represent the current state of this value.  If this is set on the background
					// queue then there's a chance, however small, that multiple updates of the backing
					// array could cause the table's metadata to be out of sync with the actual backing
					// array and cause an index error when returning cells.  Hence all this being done together
					// in the async call.
					self.objects = favoriteEvents
					self.objects.append(contentsOf: remainder)
					
					// Update the table's section/row counts and the cells' contents.
					self.tableView.reloadData()
				}
			}
		}
	}
	
	// MARK: - Segues
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
			if let indexPath = tableView.indexPathForSelectedRow {
				let object = objects[indexPath.row]
				let controller =
					(segue.destination as! UINavigationController).topViewController as! DetailViewController
				controller.detailItem = object
				controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
				controller.navigationItem.leftItemsSupplementBackButton = true
			}
		}
	}
	
	// MARK: - Table View
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		
		guard let eventCell = cell as? EventTableViewCell else {
			preconditionFailure("Table view failed to dequeue an EventTableViewCell")
		}
		
		let object = objects[indexPath.row]
		eventCell.configure(with: object)
		
		return cell
	}
	
}

// MARK: - UISearchBarDelegate
extension MasterViewController: UISearchBarDelegate {
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
	
}

extension MasterViewController: UISearchResultsUpdating {
	
	func updateSearchResults(for searchController: UISearchController) {
		// Strip out all the leading and trailing spaces.
		let whitespaceCharacterSet = CharacterSet.whitespaces
		let strippedString = searchController.searchBar.text!.trimmingCharacters(in: whitespaceCharacterSet)
		search(query: strippedString)
	}
	
}
