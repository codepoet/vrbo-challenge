//
//  EventTableViewCell.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var locationLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var eventImageView: UIImageView!
	@IBOutlet weak var favoriteImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		self.eventImageView.layer.cornerRadius = 8.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	override func prepareForReuse() {
		titleLabel?.text = ""
		locationLabel?.text = ""
		dateLabel?.text = ""
		eventImageView?.image = nil
		favoriteImageView?.image = nil
	}
	
	func configure(with event: Event) {
		titleLabel?.text = event.title
		locationLabel?.text = event.location
		dateLabel?.text = event.dateString
		
		if FavoritesManager.isFavorite(event: event) {
			favoriteImageView?.image = UIImage(named: "like-on")
		} else {
			favoriteImageView?.image = nil
		}
		
		event.fetchImage { (image, error) in
			if let error = error {
				debugPrint(error)
				return
			}
			
			self.eventImageView?.image = image
			self.eventImageView?.setNeedsDisplay()
		}
	}
}
