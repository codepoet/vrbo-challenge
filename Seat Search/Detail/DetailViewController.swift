//
//  DetailViewController.swift
//  Seat Search
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var locationLabel: UILabel!
	@IBOutlet weak var venueNameLabel: UILabel!
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var favoriteButton: UIButton!
	
	@IBAction func back(sender: Any) {
		// You'd think self.navigationController would help, right?  Nope. The split view wins.  Also, iPad.
		let nc = self.splitViewController!.viewControllers.first! as! UINavigationController
		
		// iPad idiom and the master VC is hidden, so we show it.
		if self.splitViewController!.displayMode == .primaryHidden {
			// Hello, "Objective-C Runtime Tricks", my old friend.
			// The split view hides a lovely function in the display mode button that we need. So, we just
			// grab the target/selector and fire off the message as if it were tapped.  The price we pay for custom
			// headers on view controllers.
			let target: AnyObject = self.splitViewController!.displayModeButtonItem.target!
			let sel = self.splitViewController!.displayModeButtonItem.action!
			_ = target.perform(sel)
		} else {
			// Phone idiom; on iPad this does nothing.
			nc.popToRootViewController(animated: true)
		}
	}
	
	var detailItem: Event? {
		didSet {
			// Update the view.
			configureView()
		}
	}
	
	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
			title = detail.title
			titleLabel?.text = detail.title
			dateLabel?.text = detail.dateString
			locationLabel?.text = detail.location
			venueNameLabel?.text = detail.venue

			if FavoritesManager.isFavorite(event: detail) {
				favoriteButton?.setImage(UIImage(named: "like-on"), for: .normal)
			} else {
				favoriteButton?.setImage(UIImage(named: "like-off"), for: .normal)
			}
			
			if let mapView = mapView {
				let location = VenueLocation(event: detail)
				mapView.region = mapView.regionThatFits(location.region)
				mapView.addAnnotation(location)
			}

			detail.fetchImage { (image, error) in
				if let image = image {
					self.imageView?.image = image
					self.view.setNeedsLayout()
				}
			}
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		configureView()
		
		self.imageView.layer.cornerRadius = 8.0
	}

	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		super.viewWillAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(false, animated: false)
		super.viewWillDisappear(animated)
	}
	
	@IBAction func toggleFavorite() {
		guard let detailItem = detailItem else { return }
		
		if FavoritesManager.isFavorite(event: detailItem) {
			FavoritesManager.removeFavorite(event: detailItem)
			favoriteButton.setImage(UIImage(named: "like-off"), for: .normal)
		} else {
			FavoritesManager.addFavorite(event: detailItem)
			favoriteButton.setImage(UIImage(named: "like-on"), for: .normal)
		}
	}
	
}

