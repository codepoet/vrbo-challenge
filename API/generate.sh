#!/bin/sh

fetch() {
	[ -e "Events/Sample-$2.json" ] || curl 'https://api.seatgeek.com/2/events?client_id=MTY2NzE0ODJ8MTU1ODEwMTUzNy4wNQ&q=$1' -o "Events/Sample-$2.json"
}

# Sports
fetch "Texas+Ranger"	"Rangers"
fetch "Houston+Astros"	"Astros"
fetch "Texas+Longhorns"	"Longhorns"

# Theater
fetch "Hamilton" 		"Hamilton"

# Concert
fetch "Phil+Collins"	"Collins"

# Comedy
fetch "Eddie+Izzard" 	"Izzard"

# Venue
fetch "Paramount+Theatre" 	"Paramount"
fetch "Long+Center"			"LongCenter"
fetch "Austin+City+Limits"	"ACL"

quicktype . -o "../Seat Search/Network/Events.swift" --density normal --no-enums --no-integer-strings --no-boolean-strings --url-session --type-prefix SeatGeek
