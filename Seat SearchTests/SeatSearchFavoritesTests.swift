//
//  SeatSearchFavoritesTests.swift
//  Seat SearchTests
//
//  Created by Adam Knight on 5/20/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import XCTest
@testable import Seat_Search

class SeatSearchFavoritesTests: XCTestCase {
	var event: Event!
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
		UserDefaults.standard.removeObject(forKey: "favorites")
		
		event = Event(
			eventID: 1,
			title: "Test Event",
			location: "Earth",
			venue: "The World",
			venueCoords: SeatGeekLocation(lat: 1, lon: 1),
			date: Date(),
			imageURL: nil
		)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddFavorite() {
		XCTAssert(FavoritesManager.allFavorites().count == 0)
		
		FavoritesManager.addFavorite(event: event)
		XCTAssert(FavoritesManager.allFavorites().count == 1)
		
		let favorites = FavoritesManager.allFavorites()
		XCTAssert(favorites.contains { $0.eventID == event.eventID })
    }

	func testRemoveFavorite() {
		XCTAssert(FavoritesManager.allFavorites().count == 0)
		
		FavoritesManager.addFavorite(event: event)
		XCTAssert(FavoritesManager.allFavorites().count == 1)

		FavoritesManager.removeFavorite(event: event)
		XCTAssert(FavoritesManager.allFavorites().count == 0)
	}
	
	func testIsFavorite() {
		XCTAssert(FavoritesManager.allFavorites().count == 0)
		
		FavoritesManager.addFavorite(event: event)
		XCTAssert(FavoritesManager.allFavorites().count == 1)
		
		XCTAssert(FavoritesManager.isFavorite(event: event))
	}
	
}
