//
//  Seat_SearchTests.swift
//  Seat SearchTests
//
//  Created by Adam Knight on 5/17/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import XCTest
@testable import Seat_Search

class SeatSearchModelTests: XCTestCase {
	var apiSamples: [URL] = []
	let decoder = JSONDecoder()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
		decoder.dateDecodingStrategy = .custom(betterDateDecoder)
		apiSamples = Bundle(for: SeatSearchModelTests.self).urls(forResourcesWithExtension: "json", subdirectory: nil)!
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAPIDecode() {
		for sampleURL in apiSamples {
			do {
				let data = try Data(contentsOf: sampleURL)
				let response = try decoder.decode(SeatGeekEvents.self, from: data)
				XCTAssert(response.events.count > 0)
			} catch {
				XCTFail(error.localizedDescription)
			}
		}
    }
	
	func testEventObjectInit() {
		for sampleURL in apiSamples {
			let data: Data
			let eventsResponse: [SeatGeekEvent]
			
			do {
				data = try Data(contentsOf: sampleURL)
				let response = try decoder.decode(SeatGeekEvents.self, from: data)
				XCTAssert(response.events.count > 0)
				eventsResponse = response.events
			} catch {
				XCTFail(error.localizedDescription)
				continue
			}
			
			let events = eventsResponse.map { Event(event: $0) }
			for event in events {
				XCTAssert(event.title.count > 0)
				XCTAssert(event.dateString.count > 0)
			}
		}
	}

	func testVenueLocationObjectInit() {
		for sampleURL in apiSamples {
			let data: Data
			let eventsResponse: [SeatGeekEvent]
			
			do {
				data = try Data(contentsOf: sampleURL)
				let response = try decoder.decode(SeatGeekEvents.self, from: data)
				XCTAssert(response.events.count > 0)
				eventsResponse = response.events
			} catch {
				XCTFail(error.localizedDescription)
				continue
			}
			
			let events = eventsResponse.map { Event(event: $0) }
			let venues = events.map { VenueLocation(event: $0) }
			
			XCTAssert(venues.count > 0)
			
			for venue in venues {
				XCTAssert(venue.region.center.latitude != 0)
				XCTAssert(venue.region.span.latitudeDelta >= 0.05)
				XCTAssert(venue.region.span.longitudeDelta >= 0.05)
			}
		}
	}
	
}
